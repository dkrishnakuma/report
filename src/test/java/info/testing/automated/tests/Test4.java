package info.testing.automated.tests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
/**
 * Created by gtyagi on 12/23/2016.
 */
public class Test4 {

    public  WebDriver driver4 ;
    @Test
    public void sampleTest4()
    {
         driver4=new FirefoxDriver();
         driver4.navigate().to("https://www.zalando.es");

    }
    @AfterMethod(alwaysRun = true)
    public void tearMethod()
    {

        driver4.close();
        driver4.quit();
    }
}
