package info.testing.automated.tests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
/**
 * Created by gtyagi on 12/23/2016.
 */
public class Test1 {

public static WebDriver driver1;

    @Test
    public void sampleTest1()
    {
        driver1 = new FirefoxDriver();

        driver1.navigate().to("http://www.zalando.de");


    }
@AfterMethod(alwaysRun = true)
    public void endFirsttest()
    {
       driver1.close();
        driver1.quit();
    }
}
