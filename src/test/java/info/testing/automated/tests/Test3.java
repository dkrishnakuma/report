package info.testing.automated.tests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
/**
 * Created by gtyagi on 12/23/2016.
 */
public class Test3 {

    WebDriver driver3;
    @Test
    public void sampleTest3()
    {
        driver3 = new FirefoxDriver();
        driver3.navigate().to("http://www.zalando.com");
        driver3.findElement(By.id("//input[@id = 'name'']")).clear();

    }

    @AfterMethod(alwaysRun = true)
    public void tear()
    {
        driver3.close();
        driver3.quit();


    }

}
