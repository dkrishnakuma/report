package info.testing.automated.tests;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
/**
 * Created by gtyagi on 12/28/2016.
 */
public class Test5 {
    public WebDriver driver ;
    @Test
    public void sampleTest5()
    {
        driver=new FirefoxDriver();
        driver.navigate().to("https://www.zalando.it");
    }
    @AfterMethod(alwaysRun = true)
    public void tearMethod()
    {
        driver.close();
    }
}
