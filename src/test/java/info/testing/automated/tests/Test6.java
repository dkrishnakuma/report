package info.testing.automated.tests;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
/**
 * Created by gtyagi on 1/3/2017.
 */
public class Test6 {

    public WebDriver driver;

    @Test
    public void sampleTest6()
    {
        driver = new FirefoxDriver();

        driver.navigate().to("http://www.google.com");


    }
    @AfterMethod(alwaysRun = true)
    public void endFirsttest()
    {
        driver.close();
        driver.quit();
    }
}
