package info.testing.automated.core;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
/**
 * Created by gtyagi on 12/24/2016.
 */
public class MakeMustacheReports {
    public void generateReport(String templatePath, String targetPath, Object scope) throws IOException {
        try {
            final Mustache mustache = new DefaultMustacheFactory().compile(templatePath);
            final File reportsFolder = new File(targetPath);
            reportsFolder.mkdir();
            if (reportsFolder.exists()) {
                mustache.execute(new FileWriter(targetPath + "/" + "report.html"), scope).flush();
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}