package info.testing.automated.core;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
/**
 * Created by gtyagi on 12/24/2016.
 */
public class TestReportListener implements ITestListener {
    private final ReportContent testReportContent = new ReportContent();


    @Override
    public void onTestStart(ITestResult iTestResult) {
        System.out.println("Test Sta-rted");
    }
    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        System.out.println("Test Pa-ssed");
        testReportContent.addSuccess(iTestResult);
    }
    @Override
    public void onTestFailure(ITestResult iTestResult) {
        System.out.println("Test Failed");
        testReportContent.addFailure(iTestResult);
    }
    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        System.out.println("Test Skipped");
    }
    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
    }
    @Override
    public void onStart(ITestContext iTestContext) {
    }
    @Override
    public void onFinish(ITestContext iTestContext) {

        System.out.print("Test Finished");


        testReportContent.setTotalNumberOfTests(iTestContext.getPassedTests().size() + iTestContext.getFailedTests().size());
        testReportContent.setDuration(iTestContext.getStartDate(), iTestContext.getEndDate());
        try {
            testReportContent.writeReport();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
