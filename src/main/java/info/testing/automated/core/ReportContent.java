package info.testing.automated.core;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.testng.ITestResult;

import java.util.Date;
/**
 * Created by gtyagi on 12/25/2016.
 */
public class ReportContent {
    private final TestReport testReport = new TestReport();
    private static final String REPORT_TEMPLATE_PATH = "templates/report.mustache";
    private static final String REPORT_TARGET_PATH = getReportDirectoryPath() + "/" + "reports/html";
    private static final String TARGETLOCATION = "target";
    private static String getReportDirectoryPath() {
        //final String fileSeparator = System.getProperty("file.separator");
        return System.getProperty("user.dir");
    }
    void addFailure(ITestResult iTestResult) {
        //final WebDriver driver = getRemoteDriver(iTestResult);
        //final SceenshotService screenshotService = createScreenshotService();
        final String testName = iTestResult.getMethod().getMethodName();
        final String screenshotFileName = testName + ".png";
        //screenshotService.write(screenshotFileName, ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64));
        testReport.addFailedTest(new FailedTest.Builder()
                .testName(testName)
                .stackTrace(ExceptionUtils.getStackTrace(iTestResult.getThrowable()))
                .message(iTestResult.getThrowable().getMessage().split("\\n")[0])
                .build());
    }
    void addSuccess(ITestResult iTestResult) {
        //final WebDriver driver = getRemoteDriver(iTestResult);
        //final SceenshotService screenshotService = createScreenshotService();
        final String testName = iTestResult.getMethod().getMethodName();
        final String message = "Passed";
        //screenshotService.write(screenshotFileName, ((TakesScreenshot) driver).getScreenshotAs(OutputType.BASE64));
        testReport.addPassedTest(
                new PassedTest.Builder().testName(testName).message(message).build());
    }

   /* private WebDriver getRemoteDriver(ITestResult iTestResult) {
       return  Arrays.asList(iTestResult.getParameters()).stream()
                .filter(parameter -> parameter instanceof WebDriver)
                .findFirst()
                .map(WebDriver.class::cast)
                .orElseThrow(() -> new IllegalStateException("WebDriver is not available"));
        }*/

   /* private SceenshotService createScreenshotService() {
        return new SceenshotService("reports/Sceenshots");
    }*/
    void setTotalNumberOfTests(int totalNumberOfTests) {
        testReport.setTotalNumberOfTests(totalNumberOfTests);
    }
    void setDuration(Date startDate, Date endDate) {
        testReport.setDuration(startDate, endDate);
    }
    void writeReport() throws Exception {
        final MakeMustacheReports testReportFileService = new MakeMustacheReports();
        testReportFileService.generateReport(REPORT_TEMPLATE_PATH, REPORT_TARGET_PATH, testReport);
    }
}







