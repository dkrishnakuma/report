package info.testing.automated.core;
import org.apache.commons.lang3.time.DurationFormatUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * Created by gtyagi on 12/24/2016.
 */
public class TestReport {
    final List<FailedTest> failedTests = new ArrayList<>();
    final List<PassedTest> passedTests = new ArrayList<>();
    int totalNumberOfTests;
    String durationAsText;
    public void addFailedTest(final FailedTest failedTest) {
        failedTests.add(failedTest);
    }
    public void addPassedTest(final PassedTest passedTest) {
        passedTests.add(passedTest);
    }
    public int totalNumberOfTests() {
        return totalNumberOfTests;
    }
    public int numberOfFailedTests() {
        return failedTests.size();
    }
    public int numberOfPassedTests() {
        return passedTests.size();
    }
    public int percentageOfSuccessfulTests() {
        int countOfSuccessfulTests = totalNumberOfTests() - numberOfFailedTests();
        return (int) ((((double) countOfSuccessfulTests / (double) totalNumberOfTests)) * 100);
    }
    public void setTotalNumberOfTests(int totalNumberOfTests) {
        this.totalNumberOfTests = totalNumberOfTests;
    }
    public void setDuration(Date startDate, Date endDate) {
        final long interval = endDate.getTime() - startDate.getTime();
        durationAsText = DurationFormatUtils.formatDurationWords(interval, true, true);
    }
}
