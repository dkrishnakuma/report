package info.testing.automated.core;
import org.apache.commons.lang3.RandomStringUtils;
/**
 * Created by gtyagi on 12/28/2016.
 */
public class PassedTest {
    final String testName;
    final String message;
    final String id;
    private PassedTest(final Builder builder) {
        this.testName = builder.testName;
        this.id = RandomStringUtils.randomAlphabetic(10);
        this.message = builder.message;
    }
    static class Builder {
        private String testName;
        private String message;
        Builder testName(final String testName) {
            this.testName = testName;
            return this;
        }
        Builder message(String message) {
            this.message = message;
            return this;
        }
        PassedTest build() {
            return new PassedTest(this);
        }
    }
}
